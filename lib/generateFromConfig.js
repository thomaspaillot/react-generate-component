'use babel';

import fs from 'fs';
import { index, component, container, stylesheet } from './templates';

const ensureDirectoryExists = (directoryPath) => {
  if (!fs.existsSync(directoryPath)) {
    fs.mkdirSync(directoryPath);
  }
};

const generateFromConfig = ({ directoryPath, name }) => {
  ensureDirectoryExists(directoryPath);
  fs.writeFileSync(`${directoryPath}/index.js`, index(name));
  fs.writeFileSync(`${directoryPath}/${name}Container.js`, container(name));
  fs.writeFileSync(`${directoryPath}/${name}.js`, component(name));
  fs.writeFileSync(`${directoryPath}/${name}.scss`, stylesheet(name));
};

export default generateFromConfig;
