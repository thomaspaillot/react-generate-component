'use babel';

export const index = name => `import ${name}Container from './${name}Container';

export default ${name}Container;
`;

export const component = name => `import React, { PureComponent } from 'react';
import PropTypes from 'prop-types';
import styles from './${name}.scss';

class ${name} extends PureComponent {
  render() {
    return (
      <div className={styles.container} />
    );
  }
}

${name}.propTypes = {};

export default ${name};
`;

export const container = name => `import { connect } from 'react-redux';
import ${name} from './${name}';

const mapStateToProps = state => ({});

const actionsCreator = {};

export default connect(mapStateToProps, actionsCreator)(${name});
`;

export const stylesheet = () => `.container {

}
`;
