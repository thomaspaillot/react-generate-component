'use babel';

import { CompositeDisposable, TextEditor, Disposable } from 'atom'; // eslint-disable-line
import path from 'path';
import fs from 'fs-plus';
import generateFromConfig from './generateFromConfig';

class ReactGenerateComponentView {
  static getProjectsDirectory() {
    return atom.config.get('react-component-generator.lastPath') || atom.config.get('core.projectHome') || path.join(fs.getHomeDirectory(), 'Git');
  }

  constructor() {
    this.disposables = new CompositeDisposable();

    this.element = document.createElement('div');
    this.element.classList.add('package-generator');

    this.miniEditor = new TextEditor({ mini: true });
    this.element.appendChild(this.miniEditor.element);

    this.error = document.createElement('div');
    this.error.classList.add('error');
    this.element.appendChild(this.error);

    this.message = document.createElement('div');
    this.message.classList.add('message');
    this.element.appendChild(this.message);

    this.disposables.add(atom.commands.add('atom-workspace', {
      'react-component-generator:generate': () => this.attach(),
    }));

    const blurHandler = () => this.close();
    this.miniEditor.element.addEventListener('blur', blurHandler);
    this.disposables.add(new Disposable(() => this.miniEditor.element.removeEventListener('blur', blurHandler)));
    this.disposables.add(atom.commands.add(this.element, {
      'core:confirm': () => this.confirm(),
      'core:cancel': () => this.close(),
    }));
  }

  destroy() {
    if (this.panel != null) {
      this.panel.destroy();
    }
    this.disposables.dispose();
  }

  attach() {
    if (this.panel == null) {
      this.panel = atom.workspace.addModalPanel({ item: this, visible: false });
    }
    this.previouslyFocusedElement = document.activeElement;
    this.panel.show();
    this.message.textContent = 'Enter component path';
    this.miniEditor.setText(ReactGenerateComponentView.getProjectsDirectory());
    this.miniEditor.element.focus();
  }

  close() {
    if (!this.panel.isVisible()) {
      return;
    }
    this.panel.hide();
    if (this.previouslyFocusedElement != null) {
      this.previouslyFocusedElement.focus();
    }
  }

  confirm() {
    if (this.validFolderPath()) {
      atom.config.set('react-component-generator.lastPath', path.dirname(this.getFolderPath()));
      this.createFiles(() => {
        this.close();
      });
    }
  }

  getFolderPath() {
    const folderPath = fs.normalize(this.miniEditor.getText().trim());
    const folderName = path.basename(folderPath);
    return path.join(path.dirname(folderPath), folderName);
  }

  validFolderPath() {
    if (fs.existsSync(this.getFolderPath())) {
      this.error.textContent = `Path already exists at '${this.getFolderPath()}'`;
      this.error.style.display = 'block';
      return false;
    }
    return true;
  }

  createFiles(callback) {
    const folderPath = this.getFolderPath();
    generateFromConfig({ directoryPath: folderPath, name: path.basename(folderPath) });
    callback();
  }
}

export default ReactGenerateComponentView;
