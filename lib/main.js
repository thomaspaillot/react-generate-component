'use babel';

import ReactGenerateComponentView from './react-generate-component-view';

export function activate() {
  this.view = new ReactGenerateComponentView();
}

export function deactivate() {
  if (this.view) {
    this.view.destroy();
  }
}
