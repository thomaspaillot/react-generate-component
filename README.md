React Generate Component
====================================================================================================================================================================
This is an Atom plugin to easily generate a React component inside a new folder with the corresponding index, container, component and stylesheet file

## Installation

```
$ cd ~/.atom/packages
$ git clone git@gitlab.com:thomaspaillot/react-generate-component.git
$ cd react-component-generator
$ apm install
```

## Keyboard Shortcuts

* '⌘-⌥-^-g' : Show the modal to enter the folder path
